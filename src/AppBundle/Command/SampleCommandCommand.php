<?php

namespace AppBundle\Command;

use AppBundle\Coffee\Coffee;
use AppBundle\Coffee\Decorator\WithMilk;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SampleCommandCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('decorator-sample')
            ->setDescription('...')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $coffee = new Coffee();
        $output->writeln($coffee->getPrice());
        $output->writeln($coffee->getIngredients());

        $coffeeDoubleMilk = new WithMilk(new WithMilk(new Coffee()));
        $output->writeln($coffeeDoubleMilk->getPrice());
        $output->writeln($coffeeDoubleMilk->getIngredients());

//        $output->writeln('----------------------------------------------------------');
//
//        $coffeeService = $this->getContainer()->get('coffee');
//        $output->writeln($coffeeService->getPrice());
//        $output->writeln($coffeeService->getIngredients());
    }

}
