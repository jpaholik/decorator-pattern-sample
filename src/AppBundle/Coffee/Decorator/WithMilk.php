<?php

namespace AppBundle\Coffee\Decorator;

use AppBundle\Coffee\CoffeeInterface;

class WithMilk implements CoffeeInterface {
    private $coffee;

    public function __construct(CoffeeInterface $coffee) {
        $this->coffee = $coffee;
    }

    public function getPrice() {
        return $this->coffee->getPrice() + 0.6;
    }

    public function getIngredients() {
        return $this->coffee->getIngredients().', Milk';
    }
}