<?php

namespace AppBundle\Coffee;

class Coffee implements CoffeeInterface {
    public function getPrice() {
        return 1;
    }
    public function getIngredients() {
        return 'Coffee';
    }
}

?>