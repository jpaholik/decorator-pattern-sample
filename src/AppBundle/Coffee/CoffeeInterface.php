<?php

namespace AppBundle\Coffee;

interface CoffeeInterface
{
    public function getPrice();
    public function getIngredients();
}
